<div class="cpcl">
  <?php if (!empty($settings['cpcllinks']) && user_access('administer cpcl')):?>
  <div class="cpcl-dropdown">
    <button class="cpcl-dropbtn"><img src="/<?php print $icon;?>"/></button>
    <div class="cpcl-dropdown-content">
      <?php $links = explode("\n", ctools_context_keyword_substitute($settings['cpcllinks'], array(), $context));?>
      <?php foreach ($links as $link):?>
        <?php $params = explode('|', $link);?>
        <a href="<?php print url($params[1]);?>" class="<?php print $params[2];?>"><?php print $params[0];?></a>
      <?php endforeach;?>
    </div>
  </div>
  <?php endif;?>
  <?php print $content;?>
</div>