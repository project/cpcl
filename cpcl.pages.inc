<?php

/**
 * @file
 * Page callbacks for the cpcl module.
 */
 
 /**
 * Implementation of ajax node add modal.
 */
 function cpcl_add($js = NULL, $type) {
  if (!$js) {
    return drupal_access_denied();
  }
  global $user;
  $node = (object) array(
    'uid' => $user->uid, 
    'name' => (isset($user->name) ? $user->name : ''), 
    'type' => $type, 
    'language' => LANGUAGE_NONE,
  );
  $form_state['title'] = t('Add @type', array('@type' => $type));
  $form_state['build_info']['args'] = array($node);
  $form_state['ajax'] = TRUE;
  $output = _cpcl_modal($type . '_node_form', $form_state, $node, t('Content created successfully'));
  print ajax_render($output);
  exit;
}

/**
 * Implementation of ajax node edit modal.
 */
function cpcl_edit($js = NULL, $node) {
  if (!$js) {
    return drupal_access_denied();
  }
  $form_state['ajax'] = TRUE;
  $form_state['build_info']['args'] = array($node);
  $output = _cpcl_modal($node->type . '_node_form', $form_state, $node, t('Content updated successfully'));
  print ajax_render($output);
  exit;
}

/**
 * Implementation edit in no theme.
 */
function cpcl_notheme_edit($node) {
  module_load_include('inc', 'node', 'node.pages');
  $form = drupal_get_form($node->type . '_node_form', $node);
  return drupal_render($form);
}

/**
 * Implementation of ajax node delete modal.
 */
function cpcl_delete($js = NULL, $node) {
  if (!$js) {
    return drupal_access_denied();
  }
  $form_state['title'] = t('Are you sure to delete?');
  $form_state['ajax'] = TRUE;
  $form_state['build_info']['args'] = array($node);
  $output = _cpcl_modal('node_delete_confirm', $form_state, $node, t('Content deleted successfully'));
  print ajax_render($output);
  exit;
}

/**
 * Implementation delete in no theme.
 */
function cpcl_notheme_delete($node) {
  module_load_include('inc', 'node', 'node.pages');
  $form = drupal_get_form('node_delete_confirm', $node);
  return drupal_render($form);
}

/**
 * Implementation of ajax view.
 */
function cpcl_manage($js = NULL, $type, $view , $display, $iframe = NULL) {
  if (!$js) {
    return drupal_access_denied();
  }
  ctools_include('node.pages', 'node', '');
  ctools_include('modal');
  ctools_include('ajax');
  $output = array();
  if ($iframe)
    $html = '<iframe frameborder="0" src="/cpcl/'.$view.'/'.$display.'/'.$type.'" width="100%" height="550"></iframe>';
  else {
    $arg = explode('-', $type);
    $view = views_get_view($view);
    $html = $view->preview($display, $arg);
  }
  $output[] = ctools_modal_command_display(t('Manage Content'), $html);
  print ajax_render($output);
  exit;
}

/**
 * Implementation of cpcl callback
 */
function cpcl_content($view, $display, $filter) {
  $arg = explode('-', $filter);
  $view = views_get_view($view);
  $output = $view->preview($display, $arg);
  return $output;
}

/**
 * Implementation of fieldable panel pane form
 */
function cpcl_fieldable_panels_panes_edit($js = NULL, $fpid) {
  if (!$js) {
    return drupal_access_denied();
  }
  $entity = fieldable_panels_panes_load($fpid);
  $form_state['ajax'] = TRUE;
  $form_state['entity'] = $entity;
  $form_state['add submit'] = 'yes';
  $output = _cpcl_modal('fieldable_panels_panes_entity_edit_form', $form_state, $entity, t('Content updated successfully'));
  print ajax_render($output);
  exit;
}

/**
 * Implementation of ajax ctools modal.
 */
function _cpcl_modal($form_id, $form_state, $node, $message = NULL) {
  ctools_include('node.pages', 'node', '');
  ctools_include('modal');
  ctools_include('ajax');
  $output = ctools_modal_form_wrapper($form_id, $form_state, $node);
  if (!empty($form_state['executed'])) {
    $output = array();
    ctools_add_js('ajax-responder');
    $output[] = ctools_modal_command_dismiss($message);
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }
  return $output;
}