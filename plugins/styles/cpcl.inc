<?php

/**
 * @file
 * This will allow to create custom contextual links
 */

$plugin = array(
  'title' => t('Add Contextual Link'),
  'description' => t('This will add custom contextual link for panel'),
  'render pane' => 'panels_cpcl_style_render_pane',
  'pane settings form' => 'panels_cpcl_style_settings_form',
  'render region' => 'panels_cpcl_style_render_region',
  'settings form' => 'panels_cpcl_style_settings_region_form',
);

/**
 * Render callback.
 */
function theme_panels_cpcl_style_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $settings = $vars['settings'];
  drupal_add_css(drupal_get_path('module','cpcl') . '/css/cpcl.css');
  if ($pane->type == 'fieldable_panels_pane') {
    list($panetype, $fpid) = explode(':', $pane->subtype);
    if (!empty($fpid)) {
      $entity = fieldable_panels_panes_load($fpid);
      if (!empty($entity->title)) {
        $title = $entity->title;
      }
    }
  }
  return theme('cpcl_pane_gear_dropdown', array(
    'title' => $title,
    'content' => render($content->content),
    'settings' => $settings,
    'config' => $pane->configuration,
    'context' => $vars['display']->context,
    'css' => $pane->css,
    'icon' => drupal_get_path('module','cpcl') . '/img/gear.png'
  ));
}

/**
 * Render callback.
 */
function theme_panels_cpcl_style_render_region($vars) {
  $content = implode($vars['panes']);
  $settings = $vars['settings'];
  drupal_add_css(drupal_get_path('module','cpcl') . '/css/cpcl.css');
  return theme('cpcl_region_gear_dropdown', array(
    'content' => $content,
    'settings' => $settings,
    'context' => $vars['display']->context,
    'icon' => drupal_get_path('module','cpcl') . '/img/gear.png'
  ));
}

/**
 * Settings form callback.
 */
function panels_cpcl_style_settings_form($style_settings) {
  $form['cpcllinks'] = array(
    '#title' => t('Contextual Links'),
    '#type' => 'textarea',
    '#description' => t('Please provide links separated by new line.'),
    '#default_value' => (isset($style_settings['cpcllinks'])) ? $style_settings['cpcllinks'] : '',
  );

  return $form;
}

/**
 * Settings form callback.
 */
function panels_cpcl_style_settings_region_form($style_settings) {
  $form['cpcllinks'] = array(
    '#title' => t('Contextual Links'),
    '#type' => 'textarea',
    '#description' => t('Please provide links separated by new line.'),
    '#default_value' => (isset($style_settings['cpcllinks'])) ? $style_settings['cpcllinks'] : '',
  );

  return $form;
}
